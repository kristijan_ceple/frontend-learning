import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HousingLocation } from './housing-location';
import { Guid } from 'guid-typescript';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CrudService {
  static readonly localhostNumber = "7038";
  static readonly serverUrl: string = `https://localhost:${CrudService.localhostNumber}`;
  static readonly apiUrl: string = `${CrudService.serverUrl}/api`;
  static readonly getAllApartmentsUrl: string = `${CrudService.apiUrl}/Housing`;
  static readonly getApartmentByIdUrl: string = `${CrudService.apiUrl}/Housing`;
  static readonly postApartmentUrl: string = `${CrudService.apiUrl}/Housing`;
  static readonly putApartmentUrl: string = `${CrudService.apiUrl}/Housing`;
  static readonly deleteApartmentUrl: string = `${CrudService.apiUrl}/Housing`;

  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }

  // Read all housing locations
  showAllHousingLocations() {
    console.log(`Sending a GET request to: ${CrudService.getAllApartmentsUrl}`);
    return this.http.get(`${CrudService.getAllApartmentsUrl}`);
  }

  // Read a specific housing location
  showHousingLocation(id: Guid) {
    return this.http.get(`${CrudService.getApartmentByIdUrl}/${id}`);
  }

  // Post up a housing location
  postHousingLocation(data: any) : Observable<any> {
    return this.http.post(CrudService.postApartmentUrl, data).pipe(catchError(this.error));
  }

  // Update a housing location
  putHousingLocation(id: any, data: any) : Observable<any> {
    const putUrl = `${CrudService.putApartmentUrl}/${id}`;

    return this.http
      .put(putUrl, data, {headers: this.headers})
      .pipe(catchError(this.error));
  }

  // Delete a housing location
  deleteHousingLocation(id: any) : Observable<any> {
    const deleteUrl = `${CrudService.deleteApartmentUrl}/${id}`;
    return this.http.delete(deleteUrl).pipe(catchError(this.error));
  }

  // Handle errors
  error(errorResponse: HttpErrorResponse) {
    let errorMessage = '';
    if(errorResponse.error instanceof ErrorEvent) {
      errorMessage = errorResponse.error.message;
    } else {
      errorMessage = `Error Code: ${errorResponse.status}\nMessage: ${errorResponse.message}`;
    }

    console.log(errorMessage);

    return throwError(() => {return errorMessage;});
  }
}
