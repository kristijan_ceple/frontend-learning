import { Component } from '@angular/core';
import { HousingLocation } from './housing-location';
import { CrudService } from './crud.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'My Fairhouse';

  selectedLocation: HousingLocation | undefined | null;

  constructor(private crudService: CrudService) {}

  ngOnInit() {
  }

  updateSelectedLocation(location: HousingLocation | null) {
    this.selectedLocation = location;
  }
}
