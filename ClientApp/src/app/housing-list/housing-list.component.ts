import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HousingLocation } from '../housing-location';
import { CrudService } from '../crud.service';

@Component({
  selector: 'app-housing-list',
  templateUrl: './housing-list.component.html',
  styleUrls: ['./housing-list.component.css']
})
export class HousingListComponent implements OnInit {

  @Output() locationSelectedEvent: EventEmitter<HousingLocation> = new EventEmitter<HousingLocation>();
  @Output() hideLocationDetailsEvent: EventEmitter<null> = new EventEmitter<null>();
  @Input() locationList: HousingLocation[] = [];
  results: HousingLocation[] = [];
  previousLocation: HousingLocation | undefined | null;

  constructor(private crudService: CrudService) { }

  ngOnInit(): void {
    this.crudService.showAllHousingLocations().subscribe(locations => {
      this.locationList = locations as HousingLocation[];
      this.searchHousingLocations("");
    });
  }

  searchHousingLocations(searchText: string) {
    console.log(`Searching for ${searchText}!`);

    if(!searchText) {
      // If no input is provided, we will just display all the options
      this.results = this.locationList.slice();
    }

    this.results = this.locationList.filter(
      (location: HousingLocation) => location.city.toLowerCase().includes(searchText.toLowerCase())
    );
  }

  selectHousingLocation(location: HousingLocation) {
    if(this.previousLocation !== location) {
      // New location - show its details
      this.locationSelectedEvent.emit(location);
      this.previousLocation = location;
    } else {
      // Previous location - emit the event to hide the details
      this.hideLocationDetailsEvent.emit(null);
      this.previousLocation = null;
    }

  }
}
