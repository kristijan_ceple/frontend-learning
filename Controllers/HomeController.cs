﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Web_Tut_4_JavaScript_Simple_Sandbox.Models;

namespace Web_Tut_4_JavaScript_Simple_Sandbox.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        public HomeController() {}

        [HttpGet]
        public IActionResult Index() {
            return PhysicalFile(
                Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "index.html"),
                "text/html"
                );
        }

        [HttpGet("fairhouse")]
        public IActionResult Fairhouse() {
            return new RedirectResult("/");
        }
    }
}
