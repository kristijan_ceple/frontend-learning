﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Web_Tut_4_JavaScript_Simple_Sandbox.Models;

namespace Web_Tut_4_JavaScript_Simple_Sandbox.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class HousingController : ControllerBase {
        private readonly MySandboxDatabaseContext _context;
        private readonly IMapper _mapper;

        public HousingController(MySandboxDatabaseContext context, IMapper autoMapper) {
            _context = context;
            _mapper = autoMapper;
        }

        // GET: api/Housing
        [HttpGet]
        public async Task<ActionResult<IEnumerable<HousingLocationDTO>>> GetUsers() {
            if (_context.HousingLocations == null) {
                return NotFound();
            }

            var housingLocations = await _context.HousingLocations.ToListAsync();
            var housingLocationsDTO = _mapper.Map<List<HousingLocationDTO>>(housingLocations);

            return housingLocationsDTO;
        }

        // GET: api/Housing/{id}
        [HttpGet("{id}")]
        public async Task<ActionResult<HousingLocationDTO>> GetHousingLocation(Guid id) {
            if (_context is null) {
                return NotFound();
            }

            var housingLocation = await _context.HousingLocations.FindAsync(id);

            if (housingLocation is null) {
                return NotFound();
            }

            var housingLocationDTO = _mapper.Map<HousingLocationDTO>(housingLocation);
            return housingLocationDTO;
        }

        // POST: api/Housing
        [HttpPost]
        public async Task<ActionResult<IEnumerable<HousingLocationDTO>>> postHousingLocation(HousingLocationDTO housingLocationDTO) {
            if (_context.HousingLocations is null) {
                return Problem("Entity set 'MySandboxDatabaseContext.HousingLocations' is null.");
            }

            var housingLocation = _mapper.Map<HousingLocation>(housingLocationDTO);
            if(housingLocation.Id == Guid.Empty) {
                housingLocation.Id = Guid.NewGuid();
            }

            _context.HousingLocations.Add(housingLocation);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetHousingLocation), new { id = housingLocation.Id }, _mapper.Map<HousingLocationDTO>(housingLocation));
        }

        // PUT - update the selected id
        [HttpPut("{id}")]
        public async Task<IActionResult> PutHousingLocation(Guid id, HousingLocationDTO housingLocationDto) {
            if (id != housingLocationDto.Id) {
                return BadRequest();
            }

            var housingLocation = await _context.HousingLocations.FindAsync(id);
            if (housingLocation is null) {
                return NotFound();
            }

            housingLocation = _mapper.Map(housingLocationDto, housingLocation);
            try {
                await _context.SaveChangesAsync();
            } catch(DbUpdateConcurrencyException) when (!HousingLocationExists(id)) {
                return NotFound();
            }

            return NoContent();
        }

        // DELETE - remove the selected id
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteHousingLocation(Guid id) {
            if(_context.HousingLocations == null) {
                return NotFound();
            }

            var housingLocation = await _context.HousingLocations.FindAsync(id);
            if(housingLocation is null) {
                return NotFound();
            }

            _context.HousingLocations.Remove(housingLocation);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        // Utility functions
        //private static HousingLocationDTO HousingLocationToDTO(HousingLocation housingLocation) => new HousingLocationDTO {
        //    Name = housingLocation.Name,
        //    City = housingLocation.City,
        //    State = housingLocation.State,
        //    Photo = housingLocation.Photo,
        //    AvailableUnits = housingLocation.AvailableUnits,
        //    Wifi = housingLocation.Wifi,
        //    Laundry = housingLocation.Laundry
        //};

        private bool HousingLocationExists(Guid id) {
            return (_context.HousingLocations?.Any(record => record.Id.Equals(id))).GetValueOrDefault();
        }
    }
}
