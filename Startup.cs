﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Authentication.Negotiate;
using Web_Tut_4_JavaScript_Simple_Sandbox.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.SpaServices;
using Microsoft.AspNetCore.SpaServices.AngularCli;

namespace Web_Tut_4_JavaScript_Simple_Sandbox {
    public class Startup {

        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services) {
            // Add services to the container
            services.AddAutoMapper(typeof(HousingLocationMapperProfile));
            services.AddAuthentication(NegotiateDefaults.AuthenticationScheme).AddNegotiate();
            services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            services.AddDbContext<MySandboxDatabaseContext>(opt => opt.UseSqlServer("Server=KCEPLE;Database=MySandboxDatabase;Integrated Security=True;TrustServerCertificate=True"));
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen();
            //Services.AddCors(opt => opt.AddPolicy("AllowSwagger", builder => builder.WithOrigins("https://localhost:7038/").AllowAnyHeader().AllowAnyMethod()));
            services.AddCors(opt => opt.AddPolicy("DevAngularFrontend", builder => {
                builder.WithOrigins("http://localhost:4200").AllowAnyMethod().AllowAnyHeader();
            }));
            services.AddSpaStaticFiles(configuration => configuration.RootPath = "ClientApp/dist/fairhouse");
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
            // Configure the HTTP request pipeline.
            if (env.IsDevelopment()) {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();
            app.UseRouting();

            app.UseCors("DevAngularFrontend");
            app.UseCors("AllowSwagger");

            app.UseAuthentication();
            app.UseAuthorization();

            //app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
                //endpoints.MapFallbackToFile("index.html");
            });

            app.UseSpaStaticFiles();
            app.UseSpa(spa => {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment()) {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });
        }
    }
}
