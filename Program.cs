using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace Web_Tut_4_JavaScript_Simple_Sandbox {
    public class Program {
        public static void Main(string[] args) {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => {
                    webBuilder.UseStartup<Startup>();
                });
    }
}