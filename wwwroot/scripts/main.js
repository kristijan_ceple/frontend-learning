/*
    Set up
*/
// Personalised greeting
let myButton = document.querySelector("button");
let myHeading = document.querySelector("h1");

function setUserName() {
    const myName = prompt("Please enter your name.");

    if(!myName) {
        return;
    }

    localStorage.setItem("name", myName);
    myHeading.textContent = `Sandbox Test! Welcome ${myName}!`;
}

const storedName = localStorage.getItem("name");
//console.log(`Stored name: ${storedName}`);
if(storedName) {
    document.querySelector("h1").textContent = `Sandbox Test! Welcome ${storedName}!`;
} else {
    setUserName();
}

myButton.onclick = setUserName;

// Clicks
document.querySelector("html").addEventListener("click", () => {
    console.log("Clicked!");
})

// "Dynamically" add a paragraph
const myExtraParagraph = document.createElement("p");
myExtraParagraph.textContent = "This paragraph was generated by JavaScript!";

const aElement = document.querySelector("a");
aElement.parentNode.insertBefore(myExtraParagraph, aElement);

// Enable picture changing on user clicks
const gdiLogoPath = "images/GDi_Logo_and_background.png";
const esriLogoPath = "images/esrilogo.jpg";

const myImage = document.querySelector("img");
myImage.onclick = () => {
    const mySrc = myImage.getAttribute("src");

    if(mySrc === esriLogoPath) {
        myImage.setAttribute("src", gdiLogoPath);
    } else {
        myImage.setAttribute("src", esriLogoPath);
    }
};

// Sandbox tomfoolery
alert("Hello World Alert!")

function multiply(num1, num2) {
    let result = num1 * num2;
    return result;
}