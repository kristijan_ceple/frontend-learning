﻿using System.ComponentModel.DataAnnotations;

namespace Web_Tut_4_JavaScript_Simple_Sandbox.Models {
    public class HousingLocationDTO {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Photo { get; set; }
        public short AvailableUnits { get; set; }
        public bool Wifi { get; set; }
        public bool Laundry { get; set; }
    }
}
