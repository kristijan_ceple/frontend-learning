﻿using AutoMapper;

namespace Web_Tut_4_JavaScript_Simple_Sandbox.Models {
    public class HousingLocationMapperProfile : Profile {
        public HousingLocationMapperProfile() {
            CreateMap<HousingLocation, HousingLocationDTO>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.City, opt => opt.MapFrom(src => src.City))
                .ForMember(dest => dest.State, opt => opt.MapFrom(src => src.State))
                .ForMember(dest => dest.Photo, opt => opt.MapFrom(src => src.Photo))
                .ForMember(dest => dest.AvailableUnits, opt => opt.MapFrom(src => src.AvailableUnits))
                .ForMember(dest => dest.Wifi, opt => opt.MapFrom(src => src.Wifi))
                .ForMember(dest => dest.Laundry, opt => opt.MapFrom(src => src.Laundry));

            CreateMap<HousingLocationDTO, HousingLocation>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.City, opt => opt.MapFrom(src => src.City))
                .ForMember(dest => dest.State, opt => opt.MapFrom(src => src.State))
                .ForMember(dest => dest.Photo, opt => opt.MapFrom(src => src.Photo))
                .ForMember(dest => dest.AvailableUnits, opt => opt.MapFrom(src => src.AvailableUnits))
                .ForMember(dest => dest.Wifi, opt => opt.MapFrom(src => src.Wifi))
                .ForMember(dest => dest.Laundry, opt => opt.MapFrom(src => src.Laundry));
        }
    }
}
