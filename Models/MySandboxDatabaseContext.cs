﻿using Microsoft.EntityFrameworkCore;

namespace Web_Tut_4_JavaScript_Simple_Sandbox.Models {
    public class MySandboxDatabaseContext : DbContext {
        public MySandboxDatabaseContext(DbContextOptions<MySandboxDatabaseContext> options) : base(options) {
            // Empty
        }

        public DbSet<User> Users { get; set; } = null;
        public DbSet<HousingLocation> HousingLocations { get; set; } = null;
    }
}

