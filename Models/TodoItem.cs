﻿namespace Web_Tut_4_JavaScript_Simple_Sandbox.Models
{
    public class TodoItem
    {
        public long Id { get; set; }
        public string? Name { get; set; }
        public bool IsComplete { get; set; }   
        public string? Secret { get; set; }
    }
}
