﻿using Microsoft.EntityFrameworkCore;

namespace Web_Tut_4_JavaScript_Simple_Sandbox.Models
{
    public class TodoContext : DbContext
    {
        public TodoContext(DbContextOptions<TodoContext> options) : base(options)
        {
            // Empty
        }

        public DbSet<TodoItem> TodoItems { get; set; } = null;
    }
}
