﻿using Microsoft.Identity.Client;
using System.ComponentModel.DataAnnotations;

namespace Web_Tut_4_JavaScript_Simple_Sandbox.Models {
    public class UserDTO {
        [Key]
        public string OIB { get; set; }     // This is the Id row
        public string Name { get; set; }
        public string? MiddleName { get; set; }
        public string Surname { get; set; }
        public short Age { get; set; }
        public bool IsStudent { get; set; }
        public string? Email { get; set; }
    }
}
